from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.forms.models import model_to_dict
from django.shortcuts import get_object_or_404, redirect, render
from django.utils import timezone

from .forms import AddTaskForm, LoginForm, RegisterForm, UpdateTaskForm
#from django.http import HttpResponse
from .models import ToDoItem

# 'from' keyword allows importing of necessary classes, methods and other items needed in our application. While 'import' keyword defines what we are importing from the package

# Create your views here.


def index(request):
    todoitem_list = ToDoItem.objects.filter(user_id=request.user.id)
    context = {
        'todoitem_list': todoitem_list,
        'user': request.user
    }
    return render(request, "todolist/index.html", context)


def todoitem(request, todoitem_id):
    todoitem = get_object_or_404(ToDoItem, pk=todoitem_id)
    return render(request, "todolist/todoitem.html", model_to_dict(todoitem))


def register(request):
    users = User.objects.all()
    is_user_registered = False
    context = {
        "is_user_registered": is_user_registered
    }

    if request.method == 'POST':
        form = RegisterForm(request.POST)

        if form.is_valid() == False:
            form = RegisterForm()
        else:
            username = form.cleaned_data['username']
            first_name = form.cleaned_data['first_name']
            last_name = form.cleaned_data['last_name']
            email = form.cleaned_data['email']
            password = form.cleaned_data['password']
            confirm_password = form.cleaned_data['confirm_password']

            username_duplicates = User.objects.filter(username=username)
            email_duplicates = User.objects.filter(email=email)

            print(request)
            if password == confirm_password:
                if not username_duplicates and not email_duplicates:
                    User.objects.create_user(
                        username=username, first_name=first_name, last_name=last_name, email=email, password=password)
                    is_user_registered = True
                    context = {
                        "is_user_registered": is_user_registered
                    }
                    return render(request, "todolist/register.html", context)
                else:
                    context = {
                        "error": True,
                        "error_message": "A duplicate username or email has been found, user creation unsuccessful"
                    }
            else:
                context = {
                    "error": True,
                    "error_message": "Passwords do not match"
                }

    return render(request, "todolist/register.html", context)


def change_password(request):

    is_user_authenticated = False

    user = authenticate(username="johndoe", password="john1234")
    print(user)
    if user is not None:
        authenticated_user = User.objects.get(username='johndoe')
        authenticated_user.set_password("johndoe1")
        authenticated_user.save()
        is_user_authenticated = True
        context = {
            "is_user_authenticated": is_user_authenticated
        }

        return render(request, "todolist/change_password.html", context)


def login_view(request):
    context = {}

    if request.method == 'POST':
        form = LoginForm(request.POST)

        if form.is_valid() == False:
            form = LoginForm()

        else:
            username = form.cleaned_data['username']
            password = form.cleaned_data['password']
            user = authenticate(username=username, password=password)
            context = {
                "username": username,
                "password": password
            }

            if user is not None:
                login(request, user)
                return redirect("todolist:index")
            else:
                context = {
                    "error": True
                }

    return render(request, "todolist/login.html", context)


def logout_view(request):
    logout(request)
    return redirect("todolist:index")


def add_task(request):
    context = {
        "user": request.user
    }

    if request.method == 'POST':
        form = AddTaskForm(request.POST)

        if form.is_valid() == False:
            form = AddTaskForm()
        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']

            duplicates = ToDoItem.objects.filter(task_name=task_name)

            if not duplicates:
                ToDoItem.objects.create(task_name=task_name, description=description,
                                        date_created=timezone.now(), user_id=request.user.id)
                return redirect('todolist:index')

            else:
                context = {
                    "error": True
                }

    return render(request, "todolist/add_task.html", context)


def update_task(request, todoitem_id):

    todoitem = ToDoItem.objects.filter(pk=todoitem_id)

    context = {
        "user": request.user,
        "todoitem_id": todoitem_id,
        "task_name": todoitem[0].task_name,
        "description": todoitem[0].description,
        "status": todoitem[0].status
    }

    if request.method == 'POST':
        form = UpdateTaskForm(request.POST)

        if form.is_valid() == False:
            form = UpdateTaskForm()
        else:
            task_name = form.cleaned_data['task_name']
            description = form.cleaned_data['description']
            status = form.cleaned_data['status']

            if todoitem:
                todoitem[0].task_name = task_name
                todoitem[0].description = description
                todoitem[0].status = status

                todoitem[0].save()
                return redirect("todolist:index")
            else:
                context = {
                    "error": True
                }

    return render(request, "todolist/update_task.html", context)


def delete_task(request, todoitem_id):
    todoitem = ToDoItem.objects.filter(pk=todoitem_id).delete()
    return redirect("todolist:index")
